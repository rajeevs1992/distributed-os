import socket
import sqlite3

node_list = ['192.168.0.100','192.168.0.101']
db = sqlite3.connect('db', isolation_level = None)
c = db.cursor()
try:
    c.execute('select * from data')
except:
    c.execute('create table data(key text unique, value text)');
    c.execute('create table log (seqnum INTEGER PRIMARY KEY AUTOINCREMENT, key,value)')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', 1234))
s.setblocking(1)
s.listen(2)

def send_write(node, data):
    try:
        s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s1.connect((node, 1234))
        s1.send(data)
        res = s1.recv(2048)
        s1.close()
        if res == 'OK':
            return res
        else:
            r = c.execute("select * from log where seqnum > '%s'" % (res))
            rows = r.fetchall()
            for i in rows:
                data = 'update|%s|%s|%s' % ( i[1], i[2], str(i[0]))
                send_write(node, data)
            return 'OK'
    except:
        print  "Could not connect to a host"

def broadcast(data):
    for i in node_list:
        send_write(i,data)
    return 'OK'
        
while True:    
    conn, addr = s.accept()
    data = conn.recv(2048)
    print data
    res = 'OK'
    d=data.split('|')
    if d[0] == 'write':
        c.execute("insert into log (key,value) values ('%s','%s')" % (d[1],d[2]))
        row = c.execute("select * from log order by seqnum desc")
        row = row.fetchone()
        res = broadcast('update|%s|%s|%s'%(row[1],row[2],str(row[0])))
    conn.send(res)
