import socket
import sys
import sqlite3

db = sqlite3.connect('db', isolation_level = None)
c = db.cursor()
try:
    c.execute('select * from data')
except:
    c.execute('create table data(key text unique, value text)');
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', 1234))
s.setblocking(1)
s.listen(2)
seqnum = -1
def store(key, value):
    try:
        c.execute("insert into data values('%s','%s')" % (key.strip(), value.strip()))
    except Exception as e:
        print e
        print 'store'
        c.execute("update data set value = '%s' where key = '%s'" % (value.strip(),key.strip()))
    return 'OK'

def update(key, value):
    try:
        c.execute("update data set value = '%s' where key = '%s'" % (value.strip(),key.strip()))
    except Exception as e:
        print 'update'
        print e
    return 'OK'
    
def read(key):
    try:
        r = c.execute("select value from data where key = '%s'"%(key.strip()))
        r = r.fetchone()[0]
        return r
    except Exception as e: 
        print 'read'
        return '-1'
while True:    
    conn, addr = s.accept()
    data = conn.recv(2048)
    print data
    data = data.split('|')
    res = ''
    if data[0] == 'write':
        res = store(data[1], data[2])
    elif data[0] == 'read':
        res = read(data[1])
    else:
        if seqnum == -1:
            seqnum = int(data[3])
            res = update(data[1], data[2])
        elif seqnum >= int(data[3]):
            res = 'OK'
        elif seqnum + 1 == int(data[3]):
            seqnum = int(data[3])
            res = update(data[1], data[2])
        else:
            res = str(seqnum)
    conn.send(res)
    conn.close()
