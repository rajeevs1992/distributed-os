import sys
import SocketServer
import shutil
import os
import cmd
import sqlite3
import socket
nodes = [('127.0.0.1',8001), ('127.0.0.1',8002),  ('127.0.0.1',8003),  ('127.0.0.1',8004)]
nodenum = int(sys.argv[1])
class NodeServer(SocketServer.BaseRequestHandler):
    "One instance per connection.  Override handle(self) to customize action."
    def handle(self):
        # self.request is the client connection
        data = self.request.recv(1024)# clip input at 1Kb
        GET=''
    	TYPE=''
	try:
		GET = data.split('&')
	except:
		self.request.send('')
        try:
		TYPE = GET[0].split('TYPE=')[1]
    	except:
           	pass
    	if TYPE == 'R':#If type is read
            try:
		obj = GET[1].split('OBJ=')[1]
		reqNode = GET[2].split('NODE=')[1]
    	    	self.readObj(obj)
		query_db('INSERT INTO readrq VALUES(%s, %s)' %(obj, reqNode))
            except:
		self.request.send('')
    	elif TYPE == 'W':#If type is write
		try:
			self.writeObj(GET[1].split('OBJ=')[1], GET[2].split('NEW=')[1])
		except:
       			self.request.send('')
	elif TYPE == 'O':
		obj = GET[1].split('OBJ=')[1]
		node = GET[2].split('OWN=')[1]
		query_db('INSERT INTO remoteobj VALUES(%s, %s)' %(obj, node))
	elif TYPE == 'RE':
		obj = GET[1].split('OBJ=')[1]
		node = GET[2].split('NODE=')[1]
		query_db('DELETE FROM readrq WHERE obj=%s AND node=%s' %(obj, node))
		
    def readObj(self, obj):
	value = query_db('SELECT * FROM myobj WHERE obj = %s' %(obj))[0][1]
	self.request.send(value);

    def writeObj(self, obj, new):
	query_db('UPDATE myobj set value="%s" WHERE obj=%s' %(new, obj))

class SimpleServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    # Ctrl-C will cleanly kill all spawned threads
	daemon_threads = True
    # much faster rebinding
	allow_reuse_address = True
	def __init__(self, server_address, RequestHandlerClass):
		SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)

def sendAndRecv(node, data):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		s.connect(nodes[node])
	except:
		return
	s.send(data)
	return s.recv(1024)

def query_db(query):
	con = sqlite3.connect('data');
	cur = con.cursor();
	cur.execute(query)
	con.commit()
	return cur.fetchall()
class CLI(cmd.Cmd):
	def __init__(self):
        	cmd.Cmd.__init__(self)
	        self.prompt = 'node %s>' %(nodenum)
	def do_createobject(self, arg):
		arg = arg.split(' ')
		query_db('INSERT INTO myobj VALUES(%s, "%s")' %(arg[0], arg[1]))
		data = "TYPE=O&OBJ=%s&OWN=%d" %(arg[0], nodenum)
		for index,node in enumerate(nodes):
			if index+1 != nodenum:
				sendAndRecv(index,data)
	def do_read(self, obj):
		value = query_db('SELECT * FROM myobj WHERE obj=%s' %(obj))
		if value:
			print value[0][1]
			return
		#Now we check whether we have are there any local copies of the remote obj.
		value = query_db('SELECT * FROM readcopy WHERE obj=%s' %(obj))
		if value:
			print "Reading from local remote copy"
			print value[0][1]
			return
		#Send a read request to the owner of the obj.
		value = query_db('SELECT node FROM remoteobj WHERE obj=%s' %(obj))
		if value:
			print "Sending read request to owner"
			data = "TYPE=R&OBJ=%s&NODE=%d" %(obj, nodenum)
			reply = sendAndRecv(value[0][0]-1,data)
			print reply
			query_db('INSERT INTO readcopy VALUES(%s, "%s", %s)' %(obj, reply, value[0][0]))
			return
		print "No such object"
	def do_status(self, arg):
		reply = query_db("SELECT * FROM myobj")
		print "==============================="
		print "OBJECTS OWNED"
		print "==============================="
		print "OBJ\t|\tVALUE"
		for i in reply:
			print "%s\t|\t%s" %(i[0],i[1])
		reply = query_db("SELECT * FROM remoteobj")
		print "==============================="
		print "REMOTE OBJECTS"
		print "==============================="
		print "OBJ\t|\tOWNER NODE"
		for i in reply:
			print "%s\t|\t%s" %(i[0],i[1])
		reply = query_db("SELECT * FROM readcopy")
		print "==============================="
		print "REMOTE OBJECTS WITH LOCAL COPIES"
		print "==============================="
		print "OBJ\t|\tVALUE\t|\tOWNER NODE"
		for i in reply:
			print "%s\t|\t%s\t|\t%s" %(i[0],i[1],i[2])
		print "==============================="
	def do_clear(self, arg):
		query_db('delete from myobj')
		query_db('delete from remoteobj')
		query_db('delete from readrq')
		query_db('delete from readcopy')
		return
	def do_release(self, arg):
		data = "TYPE=RE&OBJ=%s&NODE=%d" %(arg, nodenum)
		reply = query_db("SELECT node FROM readcopy WHERE obj=%s" %(arg))
		query_db('DELETE FROM readcopy WHERE obj=%s' %(arg))
		sendAndRecv(reply[0], data);
	def do_quit(self, arg):
        	sys.exit(1)
	# shortcuts
	do_q = do_quit
if __name__ == '__main__':
	pid = os.fork()
	if pid:
		server = SimpleServer(("127.0.0.1", 8000+nodenum), NodeServer)
		server.serve_forever()
	else:
		cli = CLI()
		cli.cmdloop()
