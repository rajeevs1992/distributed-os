from cmd import Cmd
import sqlite3
import socket
db = sqlite3.connect('db', isolation_level = None)
c = db.cursor()
try:
    c.execute('select * from data')
except:
    c.execute('create table data(key text unique, value text)')

class Client(Cmd):
    intro = 'CLI for Migrated OS'
    prompt = '(client)'
    node_list = ['127.0.0.1']

    def connect(self, server):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect((server, 1234))
        except:
            print "Connection failed"

    def do_write(self, s):
        '''Takes a key value pair and writes stores to local persistant storage'''
        s = s.split()
        key = s[0]
        value = s[1]
        self.do_read(key)
        try:
            c.execute("insert into data values('%s','%s')" % (key.strip(), value.strip()))
        except Exception as e:
            print e
            c.execute("update data set value = '%s' where key = '%s'" % (key.strip(),value.strip()))

    def do_print(self, s):
        '''Print value of a key if present in local storage'''
        try:
            r = c.execute("select value from data where key = '%s'"%(s))
            r = r.fetchone()[0]
            print r
        except:
            print 'Invalid variable'

    def do_defvar(self, s): 
        '''Same as write '''
        s = s.split()
        self.do_write('%s %s' % (s[0], s[1]))
        
    def do_read(self, s):
        '''Searches local storage for variable,if not found broadcasts query to all nodes'''
        res = '-1'
        key = s
        try:
            r = c.execute("select value from data where key = '%s'"%(key.strip()))
            r = r.fetchone()[0]
        except Exception as e: 
            for i in self.node_list:
                self.connect(i)
                self.socket.send('read|%s' % (key))
                res = self.socket.recv(2048)
                if res == '-1':
                    continue
                else:
                    break
            if res == '-1':
                pass
            else:
                r = c.execute("insert into data values('%s','%s')"%(key.strip(), res))
                return 0

    def do_concatate(self, s):
        self.connect()
        s = s.split()
        try:
            data[s[0]]
        except:
            self.do_defvar('%s --' % (s[0]))
        try:
            data[s[0]] = data[s[1]] + data[s[2]]
            self.do_write('%s %s'%(s[0],data[s[0]]))
        except:
            print "Invalid Variables"
s = Client()
s.cmdloop()

