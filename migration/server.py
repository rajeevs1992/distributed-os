import socket
import sqlite3

db = sqlite3.connect('db', isolation_level = None)
c = db.cursor()
try:
    c.execute('select * from data')
except:
    c.execute('create table data(key text unique, value text)');
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 1234))
s.setblocking(1)
s.listen(2)
def read(key):
    try:
        r = c.execute("select value from data where key = '%s'"%(key.strip()))
        r = r.fetchone()[0]
        c.execute("delete from data where key = '%s'"%(key.strip()))
        return r
    except Exception as e: 
        return '-1'

while True:    
    conn, addr = s.accept()
    data = conn.recv(2048)
    print data
    data = data.split('|')
    res = read(data[1])
    conn.send(res)
