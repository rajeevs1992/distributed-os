from cmd import Cmd
import sys
import socket
data = {}
class Client(Cmd):
    intro = 'Server CLI for Centralized OS'
    prompt = '(client)'
    server = '127.0.0.1'

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect((self.server, 1234))
        except:
            print "Connection failed"

    def do_EOF(self, s):
        print
        print 'Goodbye!!'
        sys.exit(0)

    def do_write(self, s):
        '''Takes a key value pair and writes stores to server'''
        self.connect()
        s = s.split()
        d =''
        try:
            d = 'write|%s|%s' % (s[0], data[s[0]])
        except:
            print "Value does not exist.Use defvar to create"
        self.socket.send(d)
        res = self.socket.recv(2048)
        if res == 'error':
            print 'error'
        else:
            print 'OK'
        return True
    def do_print(self, s):
        '''Print value of a key if present in local memory'''
        try:
            print data[s]
        except:
            pass

    def do_defvar(self, s): 
        '''Same as write but stores in local memory as well'''
        self.connect()
        s = s.split()
        data[s[0]] = s[1]
        self.do_write('%s %s' % (s[0], s[1]))
        
    def do_read(self, s):
        '''Reads the value of a key from server to local memory'''
        self.connect()
        s = s.split()
        d = 'read|%s' % (s[0])
        self.socket.send(d)
        res = self.socket.recv(2048)
        if res == 'error':
            print "Invalid read"
        else:
            data[s[0]] = res
            print 'OK'

    def do_concatate(self, s):
        '''Concantenate 2 variables into 3rd and write to server and local'''
        self.connect()
        s = s.split()
        try:
            data[s[0]]
        except:
            self.do_defvar('%s --' % (s[0]))
        try:
            data[s[0]] = data[s[1]] + data[s[2]]
            self.do_write('%s %s'%(s[0],data[s[0]]))
        except:
            print "Invalid Variables"
s = Client()   
s.cmdloop()

