import socket
import sqlite3

db = sqlite3.connect('db', isolation_level = None)
c = db.cursor()
try:
    c.execute('select * from data')
except:
    c.execute('create table data(key text unique, value text)');
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', 1234))
s.setblocking(1)
s.listen(2)
def store(key, value):
    try:
        c.execute("insert into data values('%s','%s')" % (key.strip(), value.strip()))
    except Exception as e:
        print e
        c.execute("update data set value = '%s' where key = '%s'" % (value.strip(),key.strip()))
    return 'OK'

def read(key):
    try:
        print key.strip()
        r = c.execute("select value from data where key = '%s'"%(key.strip()))
        r = r.fetchone()[0]
        return r
    except Exception as e: 
        print e
        return 'error'
while True:    
    conn, addr = s.accept()
    data = conn.recv(2048)
    data = data.split('|')
    res = ''
    if data[0] == 'write':
        res = store(data[1], data[2])
    elif data[0] == 'read':
        res = read(data[1])
    conn.send(res)
